"use strict"

//hide form element by changing the CSS style element
const hideForm = () => {
    var x = document.getElementById("formDiv");
    if (x.style.display === "none") {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }

};

//listen to the clicked event and remove its parent by using its parents parent
const deletePost = (e) => {
    const postParent = e.target.parentElement.parentElement;
    postParent.removeChild(e.target.parentElement);
};

//take the sumbitted texts and create a div for them with a delete button activating deletePost() function
const submit = () => {
    //Get name and post texts
    const name = document.postForm.name.value;
    const post = document.postForm.post.value;

    if (name === "" || post === ""){
        alert("Name and Post both need to be filled");
        return
    }

    //create a div element
    const divTag = document.createElement("div");
    divTag.classList.add("post-border");

    //create a block with <p> ans <button> elements
    const htmlBlock = `
    <h3 class="post-board post-name">
        ${name}
    </h3>
    <button onclick=deletePost(event) class="post-board post-button">
        Delete
    </button>
    <p class ="post-message">
        ${post}
    </p>
    `
    //add htmlBlock elements to the before created div
    divTag.innerHTML = htmlBlock;
    
    //add the reade created div to the "endLocation" in html code
    document.getElementById("endLocation").appendChild(divTag); 
};